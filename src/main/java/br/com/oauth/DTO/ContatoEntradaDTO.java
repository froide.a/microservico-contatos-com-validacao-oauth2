package br.com.oauth.DTO;

import br.com.oauth.Model.Contato;

public class ContatoEntradaDTO {


    private int idusuario;

    private String nome;

    private String telefone;

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Contato ConverterParaContato() {
        Contato contato = new Contato();
        contato.setIdusuario(this.getIdusuario());
        contato.setNome(this.getNome());
        contato.setTelefone(this.getTelefone());

        return contato;

    }
}






