package br.com.oauth.Repository;

import br.com.oauth.Model.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgendaRepository  extends CrudRepository<Contato, Integer> {

    List<Contato> findAllByIdusuario(int idusuario);

}
