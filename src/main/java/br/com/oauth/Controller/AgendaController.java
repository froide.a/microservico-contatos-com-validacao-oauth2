package br.com.oauth.Controller;


import br.com.oauth.DTO.ContatoEntradaDTO;
import br.com.oauth.Model.Contato;
import br.com.oauth.Security.Usuario;
import br.com.oauth.Service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RequestMapping("/agenda")
@RestController
public class AgendaController {


@Autowired
    AgendaService agendaService;

    @PostMapping
    public Contato incluirContato(@RequestBody @Valid ContatoEntradaDTO contatoEntradaDTO , @AuthenticationPrincipal Usuario usuario) {


        return  agendaService.criarNovoContato(contatoEntradaDTO, usuario);
    }



    @GetMapping(value = "/{idusuario}")
    public List<Contato> buscarContatoPeloUsuario(@RequestBody @PathVariable(name = "idusuario") int idusuario) {


        return  agendaService.buscarContato(idusuario);
    }


}
