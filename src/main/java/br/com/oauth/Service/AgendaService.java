package br.com.oauth.Service;


import br.com.oauth.DTO.ContatoEntradaDTO;
import br.com.oauth.Model.Contato;
import br.com.oauth.Repository.AgendaRepository;
import br.com.oauth.Security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgendaService {


    @Autowired
    AgendaRepository agendaRepository;

    public Contato criarNovoContato(ContatoEntradaDTO contadoEntradaDTO, Usuario usuario) {

        contadoEntradaDTO.setIdusuario(usuario.getId());

        Contato contato = contadoEntradaDTO.ConverterParaContato();
        return agendaRepository.save(contato);


    }

    public List<Contato> buscarContato(int idusuario) {

       return  agendaRepository.findAllByIdusuario(idusuario);


    }
}
