package br.com.oauth.Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message= "informe o id do usuário")
    private int idusuario;

    @NotBlank(message = "Informe o Nome")
    private String nome;

    @NotBlank(message = "Informe o Telefone")
    private String telefone;


    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getTelefone() {
        return telefone;
    }

    public Contato() {
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


    public Contato(String telefone) {
        this.telefone = telefone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
